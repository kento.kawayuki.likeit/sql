CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

create table user(
    id SERIAL primary key UNIQUE,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

insert into user(id,login_id,name,birth_date,password,create_date,update_date) values ('1','admin','�Ǘ���','1996-10-11','admin','2020-08-24','2020-08-24');
